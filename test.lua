local OrionLib = loadstring(game:HttpGet(('https://raw.githubusercontent.com/shlexware/Orion/main/source')))()
local Window = OrionLib:MakeWindow({Name = "[Pi3_Pr0] Blox Fruits OP", HidePremium = false, SaveConfig = true,IntroEnabled = false, IntroText = "Blox Fruits OP", ConfigFolder = "SaveCS_BloxPie"})

local Auto = Window:MakeTab({
	Name = "Auto Farm",
	Icon = "rbxassetid://2777725660",
	PremiumOnly = false
})

local ESP = Window:MakeTab({
	Name = "ESP",
	Icon = "rbxassetid://2777725660",
	PremiumOnly = false
})

local UI = Window:MakeTab({
	Name = "UI",
	Icon = "rbxassetid://2777725660",
	PremiumOnly = false
})

local BETA = Window:MakeTab({
	Name = "[BETA] Features",
	Icon = "rbxassetid://2777725660",
	PremiumOnly = false
})

Auto:AddButton({
	Name = "Auto Fruits",
	Callback = function()
		print("button pressed")

		if game.Workspace:FindFirstChild("Fruit") then

			game.Workspace.Fruit:Remove()

		end

		while wait(5) do



			for i,v in game.Workspace:GetChildren() do

				if v.Name:match("Fruit") then

					local pos = v.Handle.Position
					game.Players.LocalPlayer.Character:MoveTo(pos)

				end	

			end	

		end


	end    
})


BETA:AddButton({
	Name = "Auto Fruit [BETA]",
	Callback = function()
		print("button pressed")

		if game.Workspace:FindFirstChild("Fruit") then

			game.Workspace.Fruit:Remove()

		end

		while wait(5) do



			for i,v in game.Workspace:GetChildren() do

				if v.Name:match("Fruit") then

                            local b = game.Players.LocalPlayer.Character.HumanoidRootPart
							
							local old = b.Position
							
							local pos = v.Handle.Position
							b.Position = pos

							wait(1)

							b.Position = old
						    
							wait(0.5)

                 end	

			end	

		end


	end    
})

BETA:AddButton({
	Name = "Bypass Anti-Cheat",
	Callback = function()
		print("button pressed")

		local human = game.Players.LocalPlayer.Character.Humanoid:Clone()

		if human then

			human.Parent = game.Players.LocalPlayer.Character
			game.Players.LocalPlayer.Character.Humanoid:Remove()
			game.Workspace.CurrentCamera.CameraSubject = game.Players.LocalPlayer.Character

			while wait(0.05) do

				game.Players.LocalPlayer.Character.Humanoid:SetStateEnabled(15,false)

			end

		end

	end    
})

Auto:AddButton({
	Name = "No-Fog",
	Callback = function()
		print("button pressed")

		game.Lighting.FogEnd = 100000000

	end    
})

UI:AddButton({
	Name = "Fruit Shop [UI]",
	Callback = function()
		print("button pressed")

		game.Players.LocalPlayer.PlayerGui.Main.FruitShop.Visible = true


	end    
})

UI:AddButton({
	Name = "Awakening Toggle [UI]",
	Callback = function()
		print("button pressed")

		game.Players.LocalPlayer.PlayerGui.Main.FruitShop.Visible = true


	end    
})

ESP:AddButton({
	Name = "Fruit ESP",
	Callback = function()
		print("button pressed")

		if game.Workspace:FindFirstChild("Fruit") then

			game.Workspace.Fruit:Remove()

		end

		while wait(5) do



			for i,v in game.Workspace:GetChildren() do

				if v.Name:match("Fruit") then

					local h = Instance.new("Highlight")
					h.Parent = v
                    
					wait(0.5)

					h.Adornee = v

				end	

			end	

		end


	end    
})

ESP:AddButton({
	Name = "Fruit Aura",
	Callback = function()
		print("button pressed")

	    game.Players.LocalPlayer.Character.HumanoidRootPart.Size = Vector3.new(45,10,45)

	end
})

for i,v in game.Workspace:GetChildren() do

	if v.Name:match("Fruit") then

		OrionLib:MakeNotification({
			Name = "Found " .. v.Name .. "!",
			Content = "Found " .. v.Name,
			Image = "rbxassetid://4483345998",
			Time = 25
		})

	end	

end	
