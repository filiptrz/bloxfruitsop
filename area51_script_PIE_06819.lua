local Window = OrionLib:MakeWindow({Name = "AREA_51_SCRIPT", HidePremium = false, SaveConfig = true, ConfigFolder = "area51_test"})

local Auto = Window:MakeTab({
	Name = "Auto Farm",
	Icon = "rbxassetid://4483345998",
	PremiumOnly = false
})

Auto:AddButton({
	Name = "Kill Aura",
	Callback = function()
      	while wait(10) do

           for i,v in game.Workspace:GetChildren() do

              if v:FindFirstChild("Zombie") then

                 v.Zombie.Health = 0

              end

           end

        end 
  	end    
})

Auto:AddButton({
	Name = "[ADVANCED] Kill Aura",
	Callback = function()
      	while wait(0.05) do

           for i,v in game.Workspace:GetChildren() do

              if v:FindFirstChild("Zombie") then

                 v.Zombie.Health = 0

              end

           end

        end 
  	end    
})

Auto:AddButton({
	Name = "Maze TP",
	Callback = function()
      	game.Players.LocalPlayer.Character.HumanoidRootPart.Position = game.Workspace.MazeTeleport1.Position
  	end    
})

Auto:AddButton({
	Name = "Ban Client",
	Callback = function()
      	game.Players.LocalPlayer.Character.HumanoidRootPart.Position = game.Workspace.Exit.Position
  	end    
})

