local OrionLib = loadstring(game:HttpGet(('https://raw.githubusercontent.com/shlexware/Orion/main/source')))()
local Window = OrionLib:MakeWindow({Name = "[Pi3_Pr0] Blox Fruits OP", HidePremium = false, SaveConfig = true,IntroEnabled = true, IntroText = "PieBloxSCRIPTS", ConfigFolder = "SaveCS_BloxPie"})

local Auto = Window:MakeTab({
	Name = "Auto Farm",
	Icon = "rbxassetid://2777725660",
	PremiumOnly = false
})

local TP = Window:MakeTab({
	Name = "Teleport",
	Icon = "rbxassetid://2777725660",
	PremiumOnly = false
})

local UI = Window:MakeTab({
	Name = "UI",
	Icon = "rbxassetid://2777725660",
	PremiumOnly = false
})

local BETA = Window:MakeTab({
	Name = "[BETA] Features",
	Icon = "rbxassetid://2777725660",
	PremiumOnly = false
})

Auto:AddButton({
	Name = "Auto Fruits",
	Callback = function()
		print("button pressed")

		if game.Workspace:FindFirstChild("Fruit") then

			game.Workspace.Fruit:Remove()

		end

		while wait(5) do



			for i,v in game.Workspace.SpawnedFruit:GetChildren() do

				if v.Name:match("Fruit") then

					local pos = v.Handle.Position
					game.Players.LocalPlayer.Character:MoveTo(pos)

				end	

			end	

		end


	end    
})


BETA:AddButton({
	Name = "Auto Fruits [NEW]",
	Callback = function()
		print("button pressed")

		if game.Workspace:FindFirstChild("Fruit") then

			game.Workspace.Fruit:Remove()

		end

		while wait(5) do



			for i,v in game.Workspace.SpawnedFruit:GetChildren() do

				if v.Name:match("Fruit") then

					for i,b in game.Players.LocalPlayer.Character:GetChildren() do

						if b:IsA("BasePart") or b:IsA("MeshPart") or b:IsA("UnionOperation") then
							
							local pos = v.Handle.Position
							b.Position = pos
							
						end

					end

				end	

			end	

		end


	end    
})

Auto:AddButton({
	Name = "No-Fog",
	Callback = function()
		print("button pressed")

		game.Lighting.FogEnd = 100000000

	end    
})

for i,v in game.Workspace.SpawnedFruit:GetChildren() do

	if v.Name:match("Fruit") then

		OrionLib:MakeNotification({
			Name = "Found " .. v.Name .. "!",
			Content = "Found " .. v.Name,
			Image = "rbxassetid://4483345998",
			Time = 10
		})

	end	

end
